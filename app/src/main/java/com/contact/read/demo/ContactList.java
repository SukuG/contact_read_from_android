package com.contact.read.demo;

import java.util.List;

public class ContactList {

    private String id;
    private String name;
    private List<ContactEmail> emailList;
    private List<ContactPhone> phoneList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ContactEmail> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<ContactEmail> emailList) {
        this.emailList = emailList;
    }

    public List<ContactPhone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<ContactPhone> phoneList) {
        this.phoneList = phoneList;
    }
}
