package com.contact.read.demo;

public class ContactPhone {
    private String phone;

    public ContactPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
