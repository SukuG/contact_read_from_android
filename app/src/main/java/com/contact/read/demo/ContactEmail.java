package com.contact.read.demo;

public class ContactEmail {

    private String email;

    public ContactEmail(String email) {

        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
