package com.contact.read.demo;
/*
 * Created by Veera on 20/02/2018.
 */

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class ContentObserverService extends Service {

   /* private String TAG = ContentObserverService.class.getSimpleName();
    private final String e_DISPLAY_NAME = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME;

    protected MyCOntentObserver smsSentObserver = null;
    private ContactsApi contactsApi;
    private final String e_FILTER = e_DISPLAY_NAME + " NOT LIKE '%@%'";
    private final String e_ORDER = String.format("%1$s COLLATE NOCASE", e_DISPLAY_NAME);
    ArrayList<LocalContactModel> old_listContacts = new ArrayList<>();
    private Context context;
    private AppPreference preference;


    @SuppressLint("InlinedApi")
    private final String[] e_PROJECTION = {
            ContactsContract.Contacts._ID,
            e_DISPLAY_NAME,
            ContactsContract.Contacts.HAS_PHONE_NUMBER
    };


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate Called ");
        this.contactsApi = contactsApi;
        registersmsevent();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        context = getApplicationContext();

    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        //unregistersmsevent();
    }


    public void registersmsevent() {
        // TODO Auto-generated method stub
        if (smsSentObserver == null) {
            final Uri SMS_STATUS_URI = Uri.parse("content://sms");
            smsSentObserver = new MyCOntentObserver();
            // MyService.this.getContentResolver().unregisterContentObserver(smsSentObserver);
            //   unregistersmsevent();
            ContentObserverService.this.getContentResolver()
                    .registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, smsSentObserver);
        }
    }

    public void unregistersmsevent() {
        if (smsSentObserver != null) {
            ContentObserverService.this.getContentResolver().unregisterContentObserver(smsSentObserver);
            smsSentObserver = null;
        }
    }


    public class MyCOntentObserver extends ContentObserver {
        public MyCOntentObserver() {
            super(null);
        }

        private final String[] CONTACTS_PROJECTION = new String[]{
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI
        };

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Log.d(TAG, "~~~~~~" + selfChange);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            Log.d(TAG, "Added new contact" + "Called onChange");
            ArrayList<LocalContactModel> listContacts = new ArrayList<>();
            LocalContactModel contactModel = new LocalContactModel();
            listContacts.clear();
            Cursor cursor = getApplicationContext().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                    CONTACTS_PROJECTION, null, null, ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + " Desc");
            if (cursor.moveToNext()) {
                String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactDisplayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (!TextUtils.isEmpty(contactDisplayName)) {
                    listContacts.clear();
                    contactModel.setId(contactId);
                    contactModel.setName(contactDisplayName);
                    listContacts.add(GetContactsIntoArrayList(contactDisplayName));
                }
            }

            cursor.close();
            if (getStatus(old_listContacts, listContacts)) {
                old_listContacts = listContacts;
                uploadContacts(false, listContacts);
            }
        }

        public void matchContactNumbers(Map<String, LocalContactModel> contactsMap) {
            // Get numbers
            final String[] numberProjection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            };

            CursorLoader cursorLoader = new CursorLoader(getApplicationContext(),
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    numberProjection, null, null, null);

            Cursor phone = cursorLoader.loadInBackground();
            Cursor email = cursorLoader.loadInBackground();

            if (phone.moveToFirst()) {
                final int contactNumberColumnIndex =
                        phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                final int contactTypeColumnIndex =
                        phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
                final int contactIdColumnIndex =
                        phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);

                while (!phone.isAfterLast()) {
                    String number = phone.getString(contactNumberColumnIndex);
                    number = number.replaceAll("[ )-.(]", "");
                    final String contactId = phone.getString(contactIdColumnIndex);

                    LocalContactModel contact = contactsMap.get(contactId);
                    if (contact == null) {
                        continue;
                    }
                    final int type = phone.getInt(contactTypeColumnIndex);
                    String customLabel = "Custom";
                    CharSequence phoneType = ContactsContract.CommonDataKinds.Phone.getTypeLabel(getApplicationContext().getResources(), type, customLabel);
                    contact.addNumber(number, phoneType.toString());
                    phone.moveToNext();
                }
            }
            Log.d(TAG, "matchContactNumbers completed");
            phone.close();

        }

    }//End of class SmsObserver

    private boolean getStatus(ArrayList<LocalContactModel> old_listContacts, ArrayList<LocalContactModel> listContacts) {
        boolean isSameContact = false;
        if (old_listContacts == null || old_listContacts.size() == 0) {
            return true;
        }
        for (LocalContactModel newList : listContacts) {
            for (LocalContactModel oldList : old_listContacts) {
                if (!Objects.equals(newList.getName().trim(), oldList.getName().trim())) {
                    isSameContact = true;
                } else if (getEmailStatus(newList.getEmailItemList(), oldList.getEmailItemList())) {
                    isSameContact = true;
                } else if (getMobileStatus(newList.getNumbersItemList(), oldList.getNumbersItemList())) {
                    isSameContact = true;
                }
            }
        }
        return isSameContact;
    }

    private boolean getEmailStatus(RealmList<EmailModel> newEmailItemList, RealmList<EmailModel> oldEmailItemList) {
        boolean isEmailSame = false;
        if (newEmailItemList != null && newEmailItemList.size() > 0 && oldEmailItemList != null && oldEmailItemList.size() > 0) {
            for (EmailModel newEmail : newEmailItemList) {
                for (EmailModel oldEmail : oldEmailItemList) {
                    if (!Objects.equals(newEmail.getEmail().trim(), oldEmail.getEmail().trim())) {
                        isEmailSame = true;
                    }
                }
            }
        }
        return isEmailSame;
    }

    private boolean getMobileStatus(RealmList<NumbersModel> newNumberItemList, RealmList<NumbersModel> oldNumberItemList) {
        boolean isNumberSame = false;
        if (newNumberItemList != null && newNumberItemList.size() > 0 && oldNumberItemList != null && oldNumberItemList.size() > 0) {
            for (NumbersModel newNumber : newNumberItemList) {
                for (NumbersModel oldNumber : oldNumberItemList) {
                    if (!Objects.equals(newNumber.getNumber().trim(), oldNumber.getNumber().trim())) {
                        isNumberSame = true;
                    }
                }
            }
        }
        return isNumberSame;
    }

    private void uploadContacts(boolean isResponseRequired, List<LocalContactModel> localContactModelList) {
        ContactsRepo contactsRepo = AppController.getInstance().getContactsRepo();
        Observable.defer(() ->
                contactsRepo.uploadLocalContactsToServer(isResponseRequired, localContactModelList))
                .retry((integer, throwable) -> {
                    if (throwable instanceof AccessTokenException) {
                        return refreshAccessToken().toBlocking().single();
                    }
                    return false;
                })
                .subscribe(response -> {
                    if (response != null) {
                        Log.d(TAG, "New Contact uploaded : " + response.getUserMessage());
//                        contactsRepo.UpdateDataFromDB(response.getData());
                        if (response.getData() != null && response.getData().getWalletContactList() != null && response.getData().getWalletContactList().size() > 0) {
                            contactsRepo.updataParticularModel(response.getData().getWalletContactList());
                        }
                    }
                }, throwable -> {
                    Log.d(TAG, "Response : " + throwable.getMessage());
                });
    }


    protected Observable<Boolean> refreshAccessToken() {
        try {

            Log.e(TAG, "RefreshToken 1 : " + AppController.getInstance().getAppPreference().getRefreshToken());
            TinyDB tinyDB = new TinyDB(AppController.getAppContext());
            return AppController.getInstance().getAzureRepo()
                    .refreshAccessToken(tinyDB.getString(ApiConstants.TINY_DB_REFRESH_TOCKEN))
                    .compose(RxJavaUtils.applyNewObserverSchedulers())
                    .map(new Func1<AuthenticationResult, AuthenticationResult>() {
                        @Override
                        public AuthenticationResult call(AuthenticationResult authenticationResult) {
                            return authenticationResult;
                        }
                    })
                    .map(result -> {
                        if (result != null && !TextUtils.isEmpty(result.getAccessToken())) {
                            AppController.getInstance().getAppPreference().setAccessToken(result.getAccessToken());
                            AppController.getInstance().getAppPreference().setAccessTokenUpdatedDate(DateTimeUtils.getAccessTokenUpdatedDate());
                            AppController.getInstance().getAppPreference().setAccessTokenUpdatedTime(DateTimeUtils.getAccessTokenUpdatedTime());
                            tinyDB.putString(ApiConstants.TINY_DB_ACCESS_TOCKEN, result.getAccessToken());
                            Log.e(TAG, "Refreshed Token - " + result.getAccessToken());
                            return true;
                        } else
                            return false;
                    });
        } catch (Exception e) {
            e.printStackTrace();
            return Observable.just(false);
        }
    }

    public LocalContactModel GetContactsIntoArrayList(String contactName) {
        ArrayList<LocalContactModel> tempListContacts = new ArrayList<>();
        LocalContactModel tempContactModel = new LocalContactModel();
        LocalContactModel finalContactModel = new LocalContactModel();
        //List<ContactPhone> numberList = new ArrayList<>();
        RealmList<NumbersModel> numbersItemList = new RealmList<>();
        RealmList<EmailModel> emailsItemList = new RealmList<>();
        //NumbersModel tempNum = new NumbersModel();
        //EmailModel tempEma = new EmailModel();
        Log.d(TAG, "Match New Contact ID " + contactName);

        try {
            String EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,5}";
            Pattern ePattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
            Matcher eMatcher;
            ContentResolver cr = getApplicationContext().getContentResolver();
            Cursor mainCursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + " Desc");
            if (mainCursor.getCount() > 0) {
                if (mainCursor.moveToNext()) {
                    String id = mainCursor.getString(mainCursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = mainCursor.getString(mainCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);

                    tempContactModel = new LocalContactModel();
                    tempContactModel.setId(id);
                    tempContactModel.setName(name);

                    while (pCur.moveToNext()) {
                        String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phone = phone.replaceAll("\\s+", "");
                        phone = phone.replaceAll(" ", "");
                        phone = phone.replaceAll("-", "");
                        phone = phone.replaceAll("[\\p{Ps}\\p{Pe}]", ""); // Remove All ()[]{} Special character

                        //tempNum = new NumbersModel(phone, "Mobile");
                        //numbersItemList = new RealmList<>();
                        numbersItemList.add(new NumbersModel(phone, "Mobile"));
                        tempContactModel.setNumbersItemList(numbersItemList);
                    }
                    pCur.close();
                    Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (emailCur.moveToNext()) {
                        String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        email = TextUtils.getString(email);
                        eMatcher = ePattern.matcher(TextUtils.isEmpty(email) ? "" : email);
                        if (eMatcher.matches()) {
                            //tempEma = new EmailModel(email, "Home");
                            //emailsItemList = new RealmList<>();
                            emailsItemList.add(new EmailModel(email, "Home"));
                            tempContactModel.setEmailItemList(emailsItemList);
                        } else {
                            Log.e(TAG, "New Contact Email invalid : " + email);
                        }
                    }
                    emailCur.close();

                    tempListContacts.add(tempContactModel);
                }
                mainCursor.close();
            }
        } catch (Exception exc) {
            Log.e(TAG, "New Contact Sync ex : " + exc.getMessage());
        } finally {
            for (LocalContactModel newContact : tempListContacts) {
                if (contactName.equals(newContact.getName())) {
                    return newContact;
                }
            }
        }
        //end Here

        return finalContactModel;
    }*/

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
