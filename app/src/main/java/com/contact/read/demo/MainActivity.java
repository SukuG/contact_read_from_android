package com.contact.read.demo;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private int REQUEST_SINGLE_PERMISSION = 1;
    private String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED)) {
            //permission granted already
            callService();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_SINGLE_PERMISSION);
        }
    }

    private void callService() {
        displayContacts();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // If request is cancelled, the result arrays are empty.
        if (REQUEST_SINGLE_PERMISSION == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission was granted;
            callService();
        }
    }


    private void displayContacts() {

        List<ContactList> listFinal = new ArrayList<>();
        //should be null
        ContactList contactList = null;
        //don't create object here. Just declare.
        List<ContactPhone> contactPhones;
        List<ContactEmail> contactEmails;

        String id = "";
        String phone = "";
        String email = "";
        String name = "";

        ContentResolver cr = getContentResolver();
        Cursor mainCursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (mainCursor == null) {
            Log.d(TAG, "cursor null");
            return;
        }
        if (mainCursor.getCount() > 0) {
            while (mainCursor.moveToNext()) {
                contactList = new ContactList();

                id = mainCursor.getString(mainCursor.getColumnIndex(ContactsContract.Contacts._ID));
                name = mainCursor.getString(mainCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                contactList.setId(id);
                contactList.setName(name);
                //for mobile number
                if (Integer.parseInt(mainCursor.getString(mainCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor phoneCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    if (phoneCursor != null) {
                        //must create object.
                        //don't use clear method.
                        contactPhones = new ArrayList<>();
                        while (phoneCursor.moveToNext()) {
                            phone = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            phone = phone.replaceAll("\\s+", "");
                            phone = phone.replaceAll(" ", "");
                            phone = phone.replaceAll("-", "");
                            phone = phone.replaceAll("[\\p{Ps}\\p{Pe}]", "");
                            contactPhones.add(new ContactPhone(phone));

                        }
                        //add phone to list
                        contactList.setPhoneList(contactPhones);
                        phoneCursor.close();
                    }//end phone cursor
                } else {
                    //in case no mobile number then add empty array
                    contactList.setPhoneList(contactPhones = new ArrayList<>());
                }

                //for email ids
                Cursor emailCursor = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                if (emailCursor != null) {
                    //must create object.
                    //don't use clear method.
                    contactEmails = new ArrayList<>();
                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        if (isValidEmail(email)) {
                            contactEmails.add(new ContactEmail(email));

                        }
                    }
                    //add email to list
                    contactList.setEmailList(contactEmails);
                    emailCursor.close();
                }
                //add phone & email to parent list
                listFinal.add(contactList);
            }
            mainCursor.close();
        }
        Log.d(TAG, "final Contact Read List:::" + new Gson().toJson(listFinal));
    }

    private boolean isValidEmail(CharSequence email) {
        String EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,5}";
        Pattern ePattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
        Matcher eMatcher;
        eMatcher = ePattern.matcher(isEmpty(email.toString()) ? "" : email);
        return (eMatcher.matches());
    }

    private boolean isEmpty(String input) {
        return input == null || input.trim().isEmpty();
    }
}
